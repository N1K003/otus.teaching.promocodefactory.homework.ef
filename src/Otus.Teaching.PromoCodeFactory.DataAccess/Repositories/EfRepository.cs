﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseIdEntity
    {
        protected readonly DatabaseContext Context;

        public EfRepository(DatabaseContext context)
        {
            Context = context;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await Context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task<T> CreateAsync(T item)
        {
            var createdEntity = await Context.Set<T>().AddAsync(item);

            await Context.SaveChangesAsync();

            return createdEntity.Entity;
        }

        public virtual async Task<T> UpdateAsync(Guid id, T item)
        {
            var existingItem = await GetByIdAsync(id);

            if (existingItem == null)
            {
                throw new ArgumentException($"entry with id {id} not found");
            }

            item.Id = id;
            existingItem = item;

            Context.Set<T>().Update(existingItem);

            await Context.SaveChangesAsync();

            return existingItem;
        }

        public virtual async Task DeleteAsync(Guid id)
        {
            var item = await GetByIdAsync(id);

            if (item != null)
            {
                Context.Remove(item);
                await Context.SaveChangesAsync();
            }
        }
    }
}