﻿using System;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Preference> Preferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .ToTable("Employees")
                .HasKey(x => x.Id);

            modelBuilder.Entity<Employee>()
                .Property(x => x.Email)
                .HasMaxLength(50);

            modelBuilder.Entity<Employee>()
                .Property(x => x.FirstName)
                .HasMaxLength(50);

            modelBuilder.Entity<Employee>()
                .Property(x => x.LastName)
                .HasMaxLength(50);

            modelBuilder.Entity<Employee>()
                .Ignore(x => x.FullName);

            modelBuilder.Entity<Employee>()
                .HasOne(x => x.Role)
                .WithMany()
                .HasForeignKey(x => x.RoleId);

            modelBuilder.Entity<Role>()
                .ToTable("Roles")
                .HasKey(x => x.Id);

            modelBuilder.Entity<Role>()
                .Property(x => x.Name)
                .HasMaxLength(50);

            modelBuilder.Entity<Preference>()
                .ToTable("Preferences")
                .HasKey(x => x.Id);

            modelBuilder.Entity<Preference>()
                .Property(x => x.Name)
                .HasMaxLength(50);

            modelBuilder.Entity<Customer>()
                .ToTable("Customers")
                .HasKey(x => x.Id);

            modelBuilder.Entity<Customer>()
                .Property(x => x.Email)
                .HasMaxLength(50);

            modelBuilder.Entity<Customer>()
                .Property(x => x.FirstName)
                .HasMaxLength(50);

            modelBuilder.Entity<Customer>()
                .Property(x => x.DateCreated)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Customer>()
                .Property(x => x.LastName)
                .HasMaxLength(50);

            modelBuilder.Entity<Customer>()
                .Ignore(x => x.FullName);

            modelBuilder.Entity<Customer>()
                .HasMany(x => x.Promocodes)
                .WithOne()
                .HasForeignKey(x => x.Id);

            modelBuilder.Entity<PromoCode>()
                .ToTable("Promocodes")
                .HasKey(x => x.Id);

            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(x => x.PreferenceId);

            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.PartnerManager)
                .WithMany()
                .HasForeignKey(x => x.PartnerManagerId);

            modelBuilder.Entity<CustomerPreference>()
                .ToTable("CustomerPreferences")
                .HasKey(x => new {x.CustomerId, x.PreferenceId});

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany()
                .HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(x => x.PreferenceId);

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        }
    }
}