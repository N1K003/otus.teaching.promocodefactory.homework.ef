﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class AddCustomerDateCreated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                "DateCreated",
                "Customers",
                nullable: false,
                defaultValue: new DateTime(2020, 9, 16, 18, 57, 52, 582, DateTimeKind.Local).AddTicks(1992));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                "DateCreated",
                "Customers");
        }
    }
}