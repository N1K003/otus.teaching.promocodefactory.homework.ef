﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Customers",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Customers", x => x.Id); });

            migrationBuilder.CreateTable(
                "Roles",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Roles", x => x.Id); });

            migrationBuilder.CreateTable(
                "Preferences",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CustomerId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferences", x => x.Id);
                    table.ForeignKey(
                        "FK_Preferences_Customers_CustomerId",
                        x => x.CustomerId,
                        "Customers",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "Employees",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    RoleId = table.Column<Guid>(nullable: false),
                    AppliedPromocodesCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        "FK_Employees_Roles_RoleId",
                        x => x.RoleId,
                        "Roles",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "CustomerPreferences",
                table => new
                {
                    CustomerId = table.Column<Guid>(nullable: false),
                    PreferenceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPreferences", x => new {x.CustomerId, x.PreferenceId});
                    table.ForeignKey(
                        "FK_CustomerPreferences_Customers_CustomerId",
                        x => x.CustomerId,
                        "Customers",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_CustomerPreferences_Preferences_PreferenceId",
                        x => x.PreferenceId,
                        "Preferences",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "Promocodes",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    ServiceInfo = table.Column<string>(nullable: true),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PartnerName = table.Column<string>(nullable: true),
                    PartnerManagerId = table.Column<Guid>(nullable: false),
                    PreferenceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Promocodes", x => x.Id);
                    table.ForeignKey(
                        "FK_Promocodes_Customers_Id",
                        x => x.Id,
                        "Customers",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Promocodes_Employees_PartnerManagerId",
                        x => x.PartnerManagerId,
                        "Employees",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Promocodes_Preferences_PreferenceId",
                        x => x.PreferenceId,
                        "Preferences",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                "Customers",
                new[] {"Id", "Email", "FirstName", "LastName"},
                new object[] {new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), "ivan_sergeev@mail.ru", "Иван", "Петров"});

            migrationBuilder.InsertData(
                "Preferences",
                new[] {"Id", "CustomerId", "Name"},
                new object[] {new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), null, "Театр"});

            migrationBuilder.InsertData(
                "Preferences",
                new[] {"Id", "CustomerId", "Name"},
                new object[] {new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), null, "Семья"});

            migrationBuilder.InsertData(
                "Preferences",
                new[] {"Id", "CustomerId", "Name"},
                new object[] {new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), null, "Дети"});

            migrationBuilder.InsertData(
                "Roles",
                new[] {"Id", "Description", "Name"},
                new object[] {new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Администратор", "Admin"});

            migrationBuilder.InsertData(
                "Roles",
                new[] {"Id", "Description", "Name"},
                new object[] {new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "Партнерский менеджер", "PartnerManager"});

            migrationBuilder.InsertData(
                "Employees",
                new[] {"Id", "AppliedPromocodesCount", "Email", "FirstName", "LastName", "RoleId"},
                new object[]
                {
                    new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), 5, "owner@somemail.ru", "Иван", "Сергеев",
                    new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02")
                });

            migrationBuilder.InsertData(
                "Employees",
                new[] {"Id", "AppliedPromocodesCount", "Email", "FirstName", "LastName", "RoleId"},
                new object[]
                {
                    new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), 10, "andreev@somemail.ru", "Петр", "Андреев",
                    new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665")
                });

            migrationBuilder.CreateIndex(
                "IX_CustomerPreferences_PreferenceId",
                "CustomerPreferences",
                "PreferenceId");

            migrationBuilder.CreateIndex(
                "IX_Employees_RoleId",
                "Employees",
                "RoleId");

            migrationBuilder.CreateIndex(
                "IX_Preferences_CustomerId",
                "Preferences",
                "CustomerId");

            migrationBuilder.CreateIndex(
                "IX_Promocodes_PartnerManagerId",
                "Promocodes",
                "PartnerManagerId");

            migrationBuilder.CreateIndex(
                "IX_Promocodes_PreferenceId",
                "Promocodes",
                "PreferenceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "CustomerPreferences");

            migrationBuilder.DropTable(
                "Promocodes");

            migrationBuilder.DropTable(
                "Employees");

            migrationBuilder.DropTable(
                "Preferences");

            migrationBuilder.DropTable(
                "Roles");

            migrationBuilder.DropTable(
                "Customers");
        }
    }
}