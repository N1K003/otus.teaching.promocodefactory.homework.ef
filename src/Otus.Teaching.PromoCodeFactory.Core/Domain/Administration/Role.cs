﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseIdEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}