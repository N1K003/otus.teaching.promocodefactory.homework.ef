﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseIdEntity : IBaseEntity
    {
        public Guid Id { get; set; }
    }
}