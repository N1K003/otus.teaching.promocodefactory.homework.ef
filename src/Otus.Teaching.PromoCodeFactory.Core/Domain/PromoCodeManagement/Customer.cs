﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseIdEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
        public ICollection<PromoCode> Promocodes { get; set; } = new List<PromoCode>();
        public ICollection<Preference> Preferences { get; set; } = new List<Preference>();

        public DateTime DateCreated { get; set; }
    }
}