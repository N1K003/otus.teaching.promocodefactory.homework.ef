﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : BaseIdEntity
    {
        public string Name { get; set; }
    }
}