﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> : IReadOnlyRepository<T>
        where T : IBaseEntity
    {
        Task<T> CreateAsync(T item);
        Task<T> UpdateAsync(Guid id, T item);
        Task DeleteAsync(Guid id);
    }
}